package com.example.springbootfirebase;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RequestMapping
@RestController
public class WelcomeRestController {

    @GetMapping("/")
    public String ping()
    {
        return "Home page CI/CD connected!";
    }
}
